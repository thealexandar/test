-- MySQL Script generated by MySQL Workbench
-- Mon Jun 10 10:56:09 2019
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema e_diary
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema e_diary
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `e_diary` DEFAULT CHARACTER SET utf8 ;
USE `e_diary` ;

-- -----------------------------------------------------
-- Table `e_diary`.`status`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `e_diary`.`status` (
  `id_status` INT(11) NOT NULL AUTO_INCREMENT,
  `role` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id_status`))
ENGINE = InnoDB
AUTO_INCREMENT = 8
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `e_diary`.`admin`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `e_diary`.`admin` (
  `id_admin` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `surname` VARCHAR(45) NOT NULL,
  `username` VARCHAR(45) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  `status_id_status` INT(11) NOT NULL,
  PRIMARY KEY (`id_admin`),
  UNIQUE INDEX `username_UNIQUE` (`username` ASC),
  INDEX `fk_admin_status1_idx` (`status_id_status` ASC),
  CONSTRAINT `fk_admin_status1`
    FOREIGN KEY (`status_id_status`)
    REFERENCES `e_diary`.`status` (`id_status`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `e_diary`.`classes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `e_diary`.`classes` (
  `id_classes` INT(11) NOT NULL AUTO_INCREMENT,
  `class_name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id_classes`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `e_diary`.`parents`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `e_diary`.`parents` (
  `id_parents` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `surname` VARCHAR(45) NOT NULL,
  `username` VARCHAR(45) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  `status_id_status` INT(11) NOT NULL,
  PRIMARY KEY (`id_parents`),
  UNIQUE INDEX `username_UNIQUE` (`username` ASC),
  INDEX `fk_parents_status1_idx` (`status_id_status` ASC),
  CONSTRAINT `fk_parents_status1`
    FOREIGN KEY (`status_id_status`)
    REFERENCES `e_diary`.`status` (`id_status`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `e_diary`.`students`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `e_diary`.`students` (
  `id_students` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `surname` VARCHAR(45) NOT NULL,
  `jmbg` INT(11) NOT NULL,
  `parents_id_parents` INT(11) NOT NULL,
  PRIMARY KEY (`id_students`),
  INDEX `fk_students_parents1_idx` (`parents_id_parents` ASC),
  CONSTRAINT `fk_students_parents1`
    FOREIGN KEY (`parents_id_parents`)
    REFERENCES `e_diary`.`parents` (`id_parents`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `e_diary`.`subjects`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `e_diary`.`subjects` (
  `id_subjects` INT(11) NOT NULL AUTO_INCREMENT,
  `subject_name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id_subjects`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `e_diary`.`teachers`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `e_diary`.`teachers` (
  `id_teachers` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `surname` VARCHAR(45) NOT NULL,
  `username` VARCHAR(45) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  `status_id_status` INT(11) NOT NULL,
  PRIMARY KEY (`id_teachers`),
  UNIQUE INDEX `username_UNIQUE` (`username` ASC),
  INDEX `fk_teachers_status1_idx` (`status_id_status` ASC),
  CONSTRAINT `fk_teachers_status1`
    FOREIGN KEY (`status_id_status`)
    REFERENCES `e_diary`.`status` (`id_status`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `e_diary`.`grades`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `e_diary`.`grades` (
  `id_grades` INT(11) NOT NULL AUTO_INCREMENT,
  `term_1` VARCHAR(255) NOT NULL,
  `term_2` VARCHAR(255) NOT NULL,
  `term_3` VARCHAR(255) NOT NULL,
  `term_4` VARCHAR(255) NOT NULL,
  `final_part_1` VARCHAR(255) NOT NULL,
  `final_part_2` VARCHAR(255) NOT NULL,
  `subjects_id_subjects` INT(11) NOT NULL,
  `students_id_students` INT(11) NOT NULL,
  `teachers_id_teachers` INT(11) NOT NULL,
  PRIMARY KEY (`id_grades`),
  INDEX `fk_grades_subjects1_idx` (`subjects_id_subjects` ASC),
  INDEX `fk_grades_students1_idx` (`students_id_students` ASC),
  INDEX `fk_grades_teachers1_idx` (`teachers_id_teachers` ASC),
  CONSTRAINT `fk_grades_students1`
    FOREIGN KEY (`students_id_students`)
    REFERENCES `e_diary`.`students` (`id_students`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_grades_subjects1`
    FOREIGN KEY (`subjects_id_subjects`)
    REFERENCES `e_diary`.`subjects` (`id_subjects`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_grades_teachers1`
    FOREIGN KEY (`teachers_id_teachers`)
    REFERENCES `e_diary`.`teachers` (`id_teachers`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `e_diary`.`headmaster`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `e_diary`.`headmaster` (
  `id_headmaster` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `surname` VARCHAR(45) NOT NULL,
  `username` VARCHAR(45) NOT NULL,
  `status_id_status` INT(11) NOT NULL,
  PRIMARY KEY (`id_headmaster`),
  UNIQUE INDEX `username_UNIQUE` (`username` ASC),
  INDEX `fk_headmaster_status1_idx` (`status_id_status` ASC),
  CONSTRAINT `fk_headmaster_status1`
    FOREIGN KEY (`status_id_status`)
    REFERENCES `e_diary`.`status` (`id_status`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `e_diary`.`messages`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `e_diary`.`messages` (
  `id_messages` INT(11) NOT NULL AUTO_INCREMENT,
  `text_messages` VARCHAR(255) NOT NULL,
  `time` DATETIME NOT NULL,
  `message_id_teacher` INT(11) NOT NULL,
  `message_id_parent` INT(11) NOT NULL,
  INDEX `fk_messages_teachers_idx` USING BTREE (`id_messages` ASC),
  INDEX `fk_messages_parents_idx` USING BTREE (`id_messages` ASC),
  INDEX `fk_messages_parents_idx1` (`message_id_parent` ASC),
  INDEX `fk_messages_teachers_idx1` (`message_id_teacher` ASC),
  CONSTRAINT `fk_message_parent`
    FOREIGN KEY (`message_id_parent`)
    REFERENCES `e_diary`.`parents` (`id_parents`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_message_teacher`
    FOREIGN KEY (`message_id_teacher`)
    REFERENCES `e_diary`.`teachers` (`id_teachers`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `e_diary`.`news`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `e_diary`.`news` (
  `id_news` INT(11) NOT NULL AUTO_INCREMENT,
  `messages` VARCHAR(45) NULL DEFAULT NULL,
  `date_and_time` DATETIME NOT NULL,
  PRIMARY KEY (`id_news`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `e_diary`.`opendoor`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `e_diary`.`opendoor` (
  `id_opendoor` INT(11) NOT NULL AUTO_INCREMENT,
  `date_and_time` DATETIME NOT NULL,
  `teachers_id_teachers` INT(11) NOT NULL,
  `parents_id_parents` INT(11) NOT NULL,
  PRIMARY KEY (`id_opendoor`),
  INDEX `fk_opendoor_teachers1_idx` (`teachers_id_teachers` ASC),
  INDEX `fk_opendoor_parents1_idx` (`parents_id_parents` ASC),
  CONSTRAINT `fk_opendoor_parents1`
    FOREIGN KEY (`parents_id_parents`)
    REFERENCES `e_diary`.`parents` (`id_parents`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_opendoor_teachers1`
    FOREIGN KEY (`teachers_id_teachers`)
    REFERENCES `e_diary`.`teachers` (`id_teachers`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `e_diary`.`proba`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `e_diary`.`proba` (
  `id_users` INT(11) NOT NULL AUTO_INCREMENT,
  `id_status` INT(11) NOT NULL,
  PRIMARY KEY (`id_users`),
  INDEX `fk_users_status_idx` (`id_status` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `e_diary`.`students_class`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `e_diary`.`students_class` (
  `id_students_class` INT(11) NOT NULL AUTO_INCREMENT,
  `students_id_students` INT(11) NOT NULL,
  `classes_id_classes` INT(11) NOT NULL,
  PRIMARY KEY (`id_students_class`),
  INDEX `fk_students_class_students1_idx` (`students_id_students` ASC),
  INDEX `fk_students_class_classes1_idx` (`classes_id_classes` ASC),
  CONSTRAINT `fk_students_class_classes1`
    FOREIGN KEY (`classes_id_classes`)
    REFERENCES `e_diary`.`classes` (`id_classes`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_students_class_students1`
    FOREIGN KEY (`students_id_students`)
    REFERENCES `e_diary`.`students` (`id_students`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `e_diary`.`teacher_class`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `e_diary`.`teacher_class` (
  `id_teacher_class` INT(11) NOT NULL AUTO_INCREMENT,
  `teachers_id_teachers` INT(11) NOT NULL,
  `classes_id_classes` INT(11) NOT NULL,
  PRIMARY KEY (`id_teacher_class`),
  INDEX `fk_teacher_class_teachers1_idx` (`teachers_id_teachers` ASC),
  INDEX `fk_teacher_class_classes1_idx` (`classes_id_classes` ASC),
  CONSTRAINT `fk_teacher_class_classes1`
    FOREIGN KEY (`classes_id_classes`)
    REFERENCES `e_diary`.`classes` (`id_classes`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_teacher_class_teachers1`
    FOREIGN KEY (`teachers_id_teachers`)
    REFERENCES `e_diary`.`teachers` (`id_teachers`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `e_diary`.`teacher_subject`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `e_diary`.`teacher_subject` (
  `id_teacher_subject` INT(11) NOT NULL AUTO_INCREMENT,
  `teachers_id_teachers` INT(11) NOT NULL,
  `subjects_id_subjects` INT(11) NOT NULL,
  PRIMARY KEY (`id_teacher_subject`),
  INDEX `fk_teacher_subject_teachers1_idx` (`teachers_id_teachers` ASC),
  INDEX `fk_teacher_subject_subjects1_idx` (`subjects_id_subjects` ASC),
  CONSTRAINT `fk_teacher_subject_subjects1`
    FOREIGN KEY (`subjects_id_subjects`)
    REFERENCES `e_diary`.`subjects` (`id_subjects`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_teacher_subject_teachers1`
    FOREIGN KEY (`teachers_id_teachers`)
    REFERENCES `e_diary`.`teachers` (`id_teachers`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `e_diary`.`timetable`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `e_diary`.`timetable` (
  `id_timetable` INT(11) NOT NULL AUTO_INCREMENT,
  `day` VARCHAR(45) NOT NULL,
  `shift` VARCHAR(45) NOT NULL,
  `lesson_no` INT(11) NOT NULL,
  `timetable_teacher_subject` INT(11) NOT NULL,
  `timetable_classes` INT(11) NOT NULL,
  PRIMARY KEY (`id_timetable`),
  INDEX `fk_timetable_teacher_subject_idx` (`timetable_teacher_subject` ASC),
  INDEX `fk_timetable_classes_idx` (`timetable_classes` ASC),
  CONSTRAINT `fk_timetable_classes`
    FOREIGN KEY (`timetable_classes`)
    REFERENCES `e_diary`.`classes` (`id_classes`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_timetable_teacher_subject`
    FOREIGN KEY (`timetable_teacher_subject`)
    REFERENCES `e_diary`.`teacher_subject` (`id_teacher_subject`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
