  <?php
  use App\Core\DatabaseConfiguration;
  use App\Core\DatabaseConnection;
  use App\Model\AdminModel;
  use App\Model\ClassesModel;

  require_once('../core/DatabaseConfiguration.php');
  require_once('../core/DatabaseConnection.php');
  require_once('../model/AdminModel.php');
  require_once('../model/ClassesModel.php');

  $conf = new DatabaseConfiguration('localhost', 'root', '', 'e_diary');
  $conn = new DatabaseConnection($conf);

  $classModel = new ClassesModel($conn);
  
  $classes = $classModel->filter();

  $classAdd = $classModel->addClass();

  

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">

    <script type="text/javascript"> (function() { var css = document.createElement('link'); css.href = 'https://use.fontawesome.com/releases/v5.7.0/css/all.css'; css.rel = 'stylesheet'; css.type = 'text/css'; document.getElementsByTagName('head')[0].appendChild(css); })(); </script>

</head>

  <!-- Modal -->
  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header text-center">
          <h5 class="modal-title" id="exampleModalLabel">Add New Class</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="" method="post">
            <div class="form-group">
                <label for="inputState">Class Year</label>
                <select id="inputState" class="form-control" name="class_year" required>
                  <option selected>Choose...</option>
                  <option value="1">I</option>
                  <option value="2">II</option>
                  <option value="3">III</option>   
                  <option value="4">IV</option>
                  <option value="5">V</option>
                  <option value="6">VI</option>
                  <option value="7">VII</option>
                  <option value="8">VIII</option>
                </select>
              </div>
              <div class="form-group">
                <label for="inputState">Class Name</label>
                <select id="inputState" class="form-control" name="class_name" required>
                  <option selected>Choose...</option>
                  <option value="a">a</option>
                  <option value="b">b</option>
                  <option value="c">c</option>
                  <option value="d">d</option>
                  <option value="e">e</option>
                  <option value="f">f</option>
                  <option value="g">g</option>
                  <option value="h">h</option>
                </select>
              </div>
              <button name="addNew" type="submit" class="btn btn-primary float-right">Submit</button>
            </form>
          </div>
      </div>
    </div>
  </div>

<body>
<nav class="navbar fixed-top navbar-dark nav-top">
    <h2>Dashboard</h2>
    <input class="form-control search-box ml-auto" type="search" placeholder="Search" aria-label="Search">
    <ul class="navbar-nav px-3">
        <li class="nav-item text-nowrap">
            <a class="nav-link" href="#">Sign out</a>
        </li>
    </ul>
</nav>



<div class="container-fluid">
  <div class="row row-content">
    <nav class="col-md-2 d-none d-md-block sidebar pl-0 pr-0">
      <div class="sidebar-sticky">

        <ul class="nav flex-column">
            <li class="nav-item">
                <a class="nav-link active" href="#">
                <span><i class="fas fa-chart-line sidebar-icon"></i></span>
                Dashboard <span class="sr-only">(current)</span>
                </a>
            </li>

            <!-- WHAT USER ROLE "TEACHER" SEE -->

            <li class="nav-item hvr-underline-from-center">
                <a class="nav-link" href="#">
                <span><i class="material-icons sidebar-icon">school</i></span>
                Odeljenja
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">
                    <span><i class="material-icons sidebar-icon">open_in_browser</i></span>
                Otvorena Vrata
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">
                    <span><i class="material-icons sidebar-icon">message</i></span>
                Poruke
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">
                    <span><i class="material-icons sidebar-icon">schedule</i></span>
                Raspored
                </a>
            </li>

            <!-- WHAT USER ROLE "PARENT" SEE -->

            <li class="nav-item">
                <a class="nav-link" href="#">
                    <span><i class="material-icons sidebar-icon">grade</i></span>
                Ocene
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">
                    <span><i class="material-icons sidebar-icon">open_in_browser</i></span>
                    Otvorena Vrata
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">
                    <span><i class="material-icons sidebar-icon">message</i></span>
                Poruke
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">
                    <span><i class="material-icons sidebar-icon">info</i></span>
                    Obaveštenja
                </a>
            </li>

            <!-- WHAT USER ROLE "PRINCIPAL" SEE -->

            <li class="nav-item">
                <a class="nav-link" href="#">
                    <span><i class="material-icons sidebar-icon">school</i></span>
                Odeljenja
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">
                    <span><i class="material-icons sidebar-icon">subject</i></span>
                    Predmeti
                </a>
            </li>

             <!-- WHAT USER ROLE "ADMINISTRATOR" SEE -->

             <li class="nav-item">
                <a class="nav-link" href="#">
                    <span><i class="material-icons sidebar-icon">people</i></span>
                Korisnici
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">
                    <span><i class="material-icons sidebar-icon">subject</i></span>
                    Predmeti
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">
                    <span><i class="material-icons sidebar-icon">school</i></span>
                Odeljenja
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">
                    <span><i class="material-icons sidebar-icon">schedule</i></span>
                Raspored
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">
                    <span><i class="material-icons sidebar-icon">info</i></span>
                    Obaveštenja
                </a>
            </li>
        </ul>
      </div>
    </nav>
  
    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4 main">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Classes</h1>
        <!-- Button trigger modal -->
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
          Add New +
        </button>
    </div>
    <div class="col-12 border-bottom">
    <form action="" method="post">
      <div class="input-group mb-3 col-4">
        <div class="input-group-prepend">
          <label class="input-group-text" for="inputGroupSelect01">Select Year</label>
        </div>
        
            <select class="custom-select" id="inputGroupSelect01" name="value">
                <option selected>Choose...</option>

                <option value="1" class="font-weight-bold">1</option>
                <option value="2" class="font-weight-bold">2</option>
                <option value="3" class="font-weight-bold">3</option>
                <option value="4" class="font-weight-bold">4</option>
                <option value="5" class="font-weight-bold">5</option>
                <option value="6" class="font-weight-bold">6</option>
                <option value="7" class="font-weight-bold">7</option>
                <option value="8" class="font-weight-bold">8</option>

            </select>
            <div class="input-group-append">
            <button name="submit" type="submit" class="btn btn-success float-right">Confirm</button>
               
            </div>
        </form>

      </div>
    </div>
    <div class="col-12 pt-5">
    <?php
    if(is_array($classes) || is_object($classes)) {
        foreach($classes as $class){
         ?>
        <a href="#">
            <div class="card m-0 float-left main-card animated classes_card" style="background:#ffeaa7;">
                <div class="card-body text-center m-0">
                    <span><i class="material-icons card-icon">school</i></span>
                </div>
                <div class="card-footer bg-transparent text-center">
                    <h3><?php echo $class->class_name; ?></h3>
                </div>
            </div>
        </a>
    <?php 
       } 
    }
    ?>
    </div>

    </main>
<script
src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
integrity="sha256-pasqAKBDmFT4eHoN2ndd6lN370kFiGUFyTiUHWhU7k8="
crossorigin="anonymous"></script>
<script src="js/bootstrap.min.js"></script>
<script>
// $( "#updatebtn" ).click(function() {
//   alert("radi");
// });
</script>
</body>
</html>