<?php
  use App\Core\DatabaseConfiguration;
  use App\Core\DatabaseConnection;
  use App\Model\AdminModel;

  require_once('../core/DatabaseConfiguration.php');
  require_once('../core/DatabaseConnection.php');
  require_once('../model/AdminModel.php');

  $conf = new DatabaseConfiguration('localhost', 'root', '', 'e_diary');
  $conn = new DatabaseConnection($conf);

  $admin = new AdminModel($conn);

  $teacherId = isset($_GET['id']);
  $teacher = $admin->getById($teacherId);
  $teacherUpdate = $admin->updateUser($teacherId);

 


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">

    <script type="text/javascript"> (function() { var css = document.createElement('link'); css.href = 'https://use.fontawesome.com/releases/v5.7.0/css/all.css'; css.rel = 'stylesheet'; css.type = 'text/css'; document.getElementsByTagName('head')[0].appendChild(css); })(); </script>

</head>

<body>
<nav class="navbar fixed-top navbar-dark nav-top">
    <h2>Dashboard</h2>
    <input class="form-control search-box ml-auto" type="search" placeholder="Search" aria-label="Search">
    <ul class="navbar-nav px-3">
        <li class="nav-item text-nowrap">
            <a class="nav-link" href="#">Sign out</a>
        </li>
    </ul>
</nav>



<div class="container-fluid">
  <div class="row row-content">
    <nav class="col-md-2 d-none d-md-block sidebar pl-0 pr-0">
      <div class="sidebar-sticky">

        <ul class="nav flex-column">
            <li class="nav-item">
                <a class="nav-link active" href="#">
                <span><i class="fas fa-chart-line sidebar-icon"></i></span>
                Dashboard <span class="sr-only">(current)</span>
                </a>
            </li>

            <!-- WHAT USER ROLE "TEACHER" SEE -->

            <li class="nav-item hvr-underline-from-center">
                <a class="nav-link" href="#">
                <span><i class="material-icons sidebar-icon">school</i></span>
                Odeljenja
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">
                    <span><i class="material-icons sidebar-icon">open_in_browser</i></span>
                Otvorena Vrata
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">
                    <span><i class="material-icons sidebar-icon">message</i></span>
                Poruke
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">
                    <span><i class="material-icons sidebar-icon">schedule</i></span>
                Raspored
                </a>
            </li>

            <!-- WHAT USER ROLE "PARENT" SEE -->

            <li class="nav-item">
                <a class="nav-link" href="#">
                    <span><i class="material-icons sidebar-icon">grade</i></span>
                Ocene
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">
                    <span><i class="material-icons sidebar-icon">open_in_browser</i></span>
                    Otvorena Vrata
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">
                    <span><i class="material-icons sidebar-icon">message</i></span>
                Poruke
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">
                    <span><i class="material-icons sidebar-icon">info</i></span>
                    Obaveštenja
                </a>
            </li>

            <!-- WHAT USER ROLE "PRINCIPAL" SEE -->

            <li class="nav-item">
                <a class="nav-link" href="#">
                    <span><i class="material-icons sidebar-icon">school</i></span>
                Odeljenja
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">
                    <span><i class="material-icons sidebar-icon">subject</i></span>
                    Predmeti
                </a>
            </li>

             <!-- WHAT USER ROLE "ADMINISTRATOR" SEE -->

             <li class="nav-item">
                <a class="nav-link" href="#">
                    <span><i class="material-icons sidebar-icon">people</i></span>
                Korisnici
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">
                    <span><i class="material-icons sidebar-icon">subject</i></span>
                    Predmeti
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">
                    <span><i class="material-icons sidebar-icon">school</i></span>
                Odeljenja
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">
                    <span><i class="material-icons sidebar-icon">schedule</i></span>
                Raspored
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">
                    <span><i class="material-icons sidebar-icon">info</i></span>
                    Obaveštenja
                </a>
            </li>
        </ul>
      </div>
    </nav>
  
    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4 main">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Users</h1>
    </div>
      <table class="table">
          <thead class="thead-dark">
              <tr>
              <th scope="col">ID</th>
              <th scope="col">First Name</th>
              <th scope="col">Last Name</th>
              <th scope="col">Username</th>
              <th scope="col">Subject</th>
              <th scope="col">Class</th>
              <th scope="col">Action</th>
              </tr>
          </thead>
          <tbody>
              <tr>
              <form action="" method="post">
                <th scope="row"><?php ?></th>
                <td>
                  <div class="form-group">
                    <input name="name" type="text" class="form-control pl-1" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="<?php echo $teacher->name; ?>">
                  </div>
                </td>
                <td>
                  <div class="form-group">
                    <input name="surname" type="text" class="form-control pl-1" id="exampleInputPassword1" placeholder="<?php echo $teacher->surname; ?>">
                  </div>
                </td>
                <td>
                  <div class="form-group">
                    <input name="username" type="text" class="form-control pl-1" id="exampleInputPassword1" placeholder="<?php echo $teacher->username; ?>">
                  </div>
                </td>
                <td>
                  <div class="form-group">
                    <input name="pass" type="text" class="form-control pl-1" id="exampleInputPassword1" placeholder="<?php echo $teacher->password; ?>">
                  </div>
                </td>
                <!-- <td>
                  <div class="form-group">
                    <input name="status" type="text" class="form-control pl-1" id="exampleInputPassword1" placeholder="<?php echo $teacher->status; ?>">
                  </div>
                </td> -->
                <td>
                  <div class="input-group mb-3">
                    <input type="hidden" value="<?php echo $teacher->id_teachers; ?>">
                    <input type="submit" value="Submit" name="update" id="updatebtn" class="btn btn-success">
                  </div>
                </td>
              </form>
                <!-- <td>Matematika</td> -->
                <!-- <td>I-a</td> -->
              </tr>
          </tbody>
          </table>
          

    </main>
<script
src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
integrity="sha256-pasqAKBDmFT4eHoN2ndd6lN370kFiGUFyTiUHWhU7k8="
crossorigin="anonymous"></script>
<script src="js/bootstrap.min.js"></script>
<script>
// $( "#updatebtn" ).click(function() {
//   alert("radi");
// });
</script>
</body>
</html>