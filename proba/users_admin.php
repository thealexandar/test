<?php
  require_once('../core/DatabaseConfiguration.php');
  require_once('../core/DatabaseConnection.php');
  require_once('../model/AdminModel.php');

  use App\Core\DatabaseConfiguration;
  use App\Core\DatabaseConnection;
  use App\Model\AdminModel;

  $conf = new DatabaseConfiguration('localhost', 'root', '', 'e_diary');
  $conn = new DatabaseConnection($conf);

  $admin = new AdminModel($conn);
  $teachers = $admin->addUser();
  $teachersAll = $admin->getAll();
  
  $deleteUser = $admin->deleteByUserId();


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">

    <script type="text/javascript"> (function() { var css = document.createElement('link'); css.href = 'https://use.fontawesome.com/releases/v5.7.0/css/all.css'; css.rel = 'stylesheet'; css.type = 'text/css'; document.getElementsByTagName('head')[0].appendChild(css); })(); </script>

</head>

<body>
<nav class="navbar fixed-top navbar-dark nav-top">
    <h2>Dashboard</h2>
    <input class="form-control search-box ml-auto" type="search" placeholder="Search" aria-label="Search">
    <ul class="navbar-nav px-3">
        <li class="nav-item text-nowrap">
            <a class="nav-link" href="#">Sign out</a>
        </li>
    </ul>
</nav>



<div class="container-fluid">
  <div class="row row-content">
    <nav class="col-md-2 d-none d-md-block sidebar pl-0 pr-0">
      <div class="sidebar-sticky">

        <ul class="nav flex-column">
            <li class="nav-item">
                <a class="nav-link active" href="#">
                <span><i class="fas fa-chart-line sidebar-icon"></i></span>
                Dashboard <span class="sr-only">(current)</span>
                </a>
            </li>

            <!-- WHAT USER ROLE "TEACHER" SEE -->

            <li class="nav-item hvr-underline-from-center">
                <a class="nav-link" href="#">
                <span><i class="material-icons sidebar-icon">school</i></span>
                Odeljenja
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">
                    <span><i class="material-icons sidebar-icon">open_in_browser</i></span>
                Otvorena Vrata
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">
                    <span><i class="material-icons sidebar-icon">message</i></span>
                Poruke
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">
                    <span><i class="material-icons sidebar-icon">schedule</i></span>
                Raspored
                </a>
            </li>

            <!-- WHAT USER ROLE "PARENT" SEE -->

            <li class="nav-item">
                <a class="nav-link" href="#">
                    <span><i class="material-icons sidebar-icon">grade</i></span>
                Ocene
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">
                    <span><i class="material-icons sidebar-icon">open_in_browser</i></span>
                    Otvorena Vrata
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">
                    <span><i class="material-icons sidebar-icon">message</i></span>
                Poruke
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">
                    <span><i class="material-icons sidebar-icon">info</i></span>
                    Obaveštenja
                </a>
            </li>

            <!-- WHAT USER ROLE "PRINCIPAL" SEE -->

            <li class="nav-item">
                <a class="nav-link" href="#">
                    <span><i class="material-icons sidebar-icon">school</i></span>
                Odeljenja
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">
                    <span><i class="material-icons sidebar-icon">subject</i></span>
                    Predmeti
                </a>
            </li>

             <!-- WHAT USER ROLE "ADMINISTRATOR" SEE -->

             <li class="nav-item">
                <a class="nav-link" href="#">
                    <span><i class="material-icons sidebar-icon">people</i></span>
                Korisnici
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">
                    <span><i class="material-icons sidebar-icon">subject</i></span>
                    Predmeti
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">
                    <span><i class="material-icons sidebar-icon">school</i></span>
                Odeljenja
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">
                    <span><i class="material-icons sidebar-icon">schedule</i></span>
                Raspored
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">
                    <span><i class="material-icons sidebar-icon">info</i></span>
                    Obaveštenja
                </a>
            </li>
        </ul>
      </div>
    </nav>

  <!-- Modal -->
  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header text-center">
          <h5 class="modal-title" id="exampleModalLabel">Add New User</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
        <form action="" method="post">
        <!-- <div class="form-group">
            <label for="inputState">Role</label>
            <select id="inputState" class="form-control" required>
              <option selected>Choose...</option>
              <option>Teacher</option>
              <option>Parent</option>
              <option>Headmaster</option>
            </select>
          </div> -->
          <div class="form-group">
            <label for="exampleInputEmail1">First Name</label>
            <input name="name" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter First Name" required>
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1">Last Name</label>
            <input name="surname" type="text" class="form-control" id="exampleInputPassword1" placeholder="Enter Last Name" required>
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1">Username</label>
            <input name="username" type="text" class="form-control" id="exampleInputPassword1" placeholder="Username" required>
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1">Password</label>
            <input name="pass" type="text" class="form-control" id="exampleInputPassword1" placeholder="Password" required>
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1">Status</label>
            <input name="status" type="text" class="form-control" id="exampleInputPassword1" placeholder="Password" required>
          </div>
<!--           <div class="form-group">
            <label for="inputState">Subject</label>
            <select id="inputState" class="form-control" required>
              <option selected>Choose...</option>
              <option>Teacher</option>
              <option>Parent</option>
              <option>Headmaster</option>
            </select>
          </div>
          <div class="form-group">
            <label for="inputState">Class</label>
            <select id="inputState" class="form-control" required>
              <option selected>Choose...</option>
              <option>Ia</option>
              <option>Ib</option>
              <option>Ic</option>
            </select>
          </div> -->
          <button name="submit" type="submit" class="btn btn-primary float-right">Submit</button>
        </form>
          </div>
          <!-- <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
        </div> -->
      </div>
    </div>
  </div>
  
    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4 main">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Users</h1>
        <!-- Button trigger modal -->
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
      Add New +
    </button>
    </div>
      <table class="table">
          <thead class="thead-dark">
              <tr>
              <th scope="col">ID</th>
              <th scope="col">First Name</th>
              <th scope="col">Last Name</th>
              <th scope="col">Username</th>
              <th scope="col">Subject</th>
              <th scope="col">Class</th>
              <th scope="col">Action</th>
              </tr>
          </thead>
          <tbody>
            <?php foreach($teachersAll as $teacher):?>
              <tr>
              <th scope="row"><?php echo $teacher->id_teachers;?></th>
                <td><?php echo $teacher->name;?></td>
                <td><?php echo $teacher->surname;?></td>
                <td><?php echo $teacher->username;?></td>
                <td><?php echo $teacher->password;?></td>
                <!-- <td>Matematika</td> -->
                <!-- <td>I-a</td> -->
                <td>
                  <form action="" method="post">
                    <div class="input-group mb-3">

                      <a href="users_update_admin.php?id=<?php echo $teacher->id_teachers; ?>" class="btn btn-success mr-1">Update</a>
                      <input type="hidden" name="teacher_id" value="<?php echo $teacher->id_teachers;?>">
                      <input type="submit" name="delete" value="Delete" id="delete" class="btn btn-danger">
                    </div>
                  </form>
                </td>
              </tr>

              
            <?php endforeach;?>
          </tbody>
          </table>
          

    </main>
<script
src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
integrity="sha256-pasqAKBDmFT4eHoN2ndd6lN370kFiGUFyTiUHWhU7k8="
crossorigin="anonymous"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>