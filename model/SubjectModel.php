<?php  
	namespace App\Model;
	use App\Core\DatabaseConnection;

 	//ovde ide autolaod 
 	require_once('../core/DatabaseConfiguration.php');
	require_once('../core/DatabaseConnection.php');


	class SubjectsModal{

			private $dbc;

			public function __construct(DatabaseConnection $dbc){
				$this->dbc = $dbc;
			}

			public function addSubject(){
				if(isset($_POST['submit'])){
					$this->subjects = $_POST['subject'];

					$sql = 'INSERT INTO e_diary.subjects (subjects_name) VALUES ( ?;';
					$prep = $this->dbc->getConnection()->prepare($sql);
					$res = $prep->execute([$this->subject]);
				}
			}

			/*public function getById(int $userId) {
				$this->userId = $_GET['id'];
					$sql	= 'SELECT * FROM e_diary.teachers WHERE id_teachers = ?';
					$prep	= $this->dbc->getConnection()->prepare($sql);
					$res	= $prep->execute([$this->userId]);
					$user	= NULL;
					if($res) {
						$user = $prep->fetch(\PDO::FETCH_OBJ);
					}
					return $user;

			}*/

			public function getAllSubjects(){
				$sql = 'SELECT * FROM e_diary.subjects';
				$prep = $this->dbc->getConnection()->prepare($sql);
				$res = $prep->execute();
                $subjects = [];
                if($res){
                    $subjects = $prep->fetchAll(\PDO::FETCH_OBJ);
                }
                return $subjects;

            }

			public function deleteByUSubjectId(){

				if(isset($_POST['delete'])) {
					$subjectId = $_POST['subject_id'];
					$sql = 'DELETE FROM e_diary.subjects WHERE id_subjects=?;';
					$prep = $this->dbc->getConnection()->prepare($sql);
					return  $prep->execute([$subjectId]);
					
				}

			}
			

	}


 