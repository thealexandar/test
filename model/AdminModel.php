<?php  
	namespace App\Model;
	use App\Core\DatabaseConnection;

 	//ovde ide autolaod 
 	require_once('../core/DatabaseConfiguration.php');
	require_once('../core/DatabaseConnection.php');


	class AdminModel{

			private $dbc;

			public function __construct(DatabaseConnection $dbc){
				$this->dbc = $dbc;
			}

			public function addUser(){
				if(isset($_POST['submit'])){
					$this->name = $_POST['name'];
					$this->surname = $_POST['surname'];
					$this->username = $_POST['username'];
					$this->pass =$_POST['pass'];
					$this->status = $_POST['status'];

					$sql = 'INSERT INTO e_diary.teachers (name, surname, username, password, status_id_status) VALUES ( ?,?,?,?,?)';
					$prep = $this->dbc->getConnection()->prepare($sql);
					$res = $prep->execute([$this->name, $this->surname, $this->username, $this->pass, $this->status]);
				}
			}

			public function getById(int $userId) {
				$this->userId = $_GET['id'];
					$sql	= 'SELECT * FROM e_diary.teachers WHERE id_teachers = ?';
					$prep	= $this->dbc->getConnection()->prepare($sql);
					$res	= $prep->execute([$this->userId]);
					$user	= NULL;
					if($res) {
						$user = $prep->fetch(\PDO::FETCH_OBJ);
					}
					return $user;

			}

			public function getAll(){
				$sql = 'SELECT * FROM e_diary.teachers';
				$prep = $this->dbc->getConnection()->prepare($sql);
				$res = $prep->execute();
                $teachers = [];
                if($res){
                    $teachers = $prep->fetchAll(\PDO::FETCH_OBJ);
                }
                return $teachers;
            }

			public function deleteByUserId(){
				if(isset($_POST['delete'])) {
					$teachersId = $_POST['teacher_id'];
					$sql = 'DELETE FROM e_diary.teachers WHERE id_teachers=?;';
					$prep = $this->dbc->getConnection()->prepare($sql);
					return  $prep->execute([$teachersId]);
				}
			}
			public function updateUser($teachersId){
				if(isset($_POST['update'])){
					$this->teachersId = isset($_POST['teacher_id']);
					$this->id_teachers = isset($_POST['id_teachers']);
					$this->name = isset($_POST['name']);
					$this->surname = isset($_POST['surname']);
					$this->username = isset($_POST['username']);
					$this->pass = isset($_POST['pass']);
					// $this->status = isset($_POST['status']);
					
					$sql = 'UPDATE e_diary.teachers SET name = ?, surname = ?, username = ?, passsword = ? WHERE id_teachers = ?;';
					$prep = $this->dbc->getConnection()->prepare($sql);
					return $prep->execute([$this->name, $this->surname, $this->username, $this->pass, $this->teachersId]);
				}

			}
	}

//UPDATE `e_diary`.`teachers` SET `name` = 'qqw' WHERE (`id_teachers` = '42');
 