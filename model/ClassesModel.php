<?php  
	namespace App\Model;
	use App\Core\DatabaseConnection;

 	//ovde ide autolaod 
 	require_once('../core/DatabaseConfiguration.php');
	require_once('../core/DatabaseConnection.php');


	class ClassesModel{

			private $dbc;

			public function __construct(DatabaseConnection $dbc){
				$this->dbc = $dbc;
			}

			public function addClass(){
				if(isset($_POST['addNew'])){
					$year = $_POST['class_year'];
					$name = $_POST['class_name'];

					$full_name = $year . $name;
					
					$sql = 'INSERT INTO e_diary.classes (class_name) VALUES (?)';
					$prep = $this->dbc->getConnection()->prepare($sql);
					$res = $prep->execute([$full_name]);
					header("Location: classes_admin.php");
				}
			}

			public function getById(int $classId) {
				$this->classId = $_GET['id'];
					$sql	= 'SELECT * FROM e_diary.classes WHERE id_classes = ?;';
					$prep	= $this->dbc->getConnection()->prepare($sql);
					$res	= $prep->execute([$this->classId]);
					$class	= NULL;
					if($res) {
						$class = $prep->fetch(\PDO::FETCH_OBJ);
					}
					return $class;
			}

			public function getAll(){
				$sql = 'SELECT * FROM e_diary.classes';
				$prep = $this->dbc->getConnection()->prepare($sql);
				$res = $prep->execute();
                $classes = [];
                if($res){
                    $classes = $prep->fetchAll(\PDO::FETCH_OBJ);
                }
                return $classes;

			}
			
			public function getByName(string $classYear) {
				$sql = 'SELECT class_year FROM e_diary.classes WHERE class_year = ?;';
				$prep = $this->dbc->getConnection()->prepare($sql);
				$res = $prep->execute([$classYear]);
				$year = NULL;
				if($res) {
					$year = $prep->fetch(\PDO::FETCH_OBJ);
				}
				return $year;
			}

			public function deleteByClassId(){

				if(isset($_POST['delete'])) {
					$classId = $_POST['classes_id'];
					$sql = 'DELETE FROM e_diary.classes WHERE id_classes=?;';
					$prep = $this->dbc->getConnection()->prepare($sql);
					return  $prep->execute([$classes_id]);
					
				}

			}
			
			public function filter() {
				if(isset($_POST['submit'])) {
					$q = isset($_POST['value']);
					switch($q) {
						case 1:
							$sql = "SELECT * FROM e_diary.classes WHERE class_name LIKE '%1%'";
							break;
						case 2:
							$sql = "SELECT * FROM e_diary.classes WHERE class_name LIKE '%2%'";
							break;
						case 3:
							$sql = "SELECT * FROM e_diary.classes WHERE class_name LIKE '%3%'";
							break;
						case 4:
							$sql = "SELECT * FROM e_diary.classes WHERE class_name LIKE '%4%'";
							break;
						case 5:
							$sql = "SELECT * FROM e_diary.classes WHERE class_name LIKE '%5%'";
							break;
						case 6:
							$sql = "SELECT * FROM e_diary.classes WHERE class_name LIKE '%6%'";
							break;
						case 7:
							$sql = "SELECT * FROM e_diary.classes WHERE class_name LIKE '%7%'";
							break;
						case 8:
							$sql = "SELECT * FROM e_diary.classes WHERE class_name LIKE '%8%'";
							break;
					}
					$prep = $this->dbc->getConnection()->prepare($sql);
					$res = $prep->execute();
					$classes = [];
					if($res) {
						$classes = $prep->fetchAll(\PDO::FETCH_OBJ);
					}
					return $classes;
				}
			}

	}


 